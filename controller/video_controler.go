package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"gitlab.com/mekuanint12/simple_api_gin/entity"
	"gitlab.com/mekuanint12/simple_api_gin/service"
	"gitlab.com/mekuanint12/simple_api_gin/validators"
)

var validate *validator.Validate

type VideoController interface {
	FindAll() []entity.Video
	Save(ctx *gin.Context) error
}

type controller struct {
	service service.VideoService
}

func New(service service.VideoService) VideoController {
	validate = validator.New()
	validate.RegisterValidation("is-cool", validators.ValidateCoolTitle)
	return &controller{
		service: service,
	}
}

func (c *controller) FindAll() []entity.Video {
	return c.service.FindAll()
}
func (c *controller) Save(ctx *gin.Context) error {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}
	c.service.Save(video)
	return nil
}
